﻿using Microsoft.AspNetCore.Mvc;

namespace Scorina.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}